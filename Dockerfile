FROM node:14

WORKDIR /usr/src/app

COPY package*.json ./

COPY *.json ./

RUN npm install

COPY src/ ./src/

COPY public/ ./public/

EXPOSE 3000

CMD ["npm", "run", "dev"]